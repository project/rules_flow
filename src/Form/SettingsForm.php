<?php

namespace Drupal\rules_flow\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure rules_flow settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'rules_flow_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['rules_flow.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $config=$this->config('rules_flow.settings');

    if(TRUE || $config->isNew()){

      $config->set("host_name",$config->get("host_name_default"));
      $config->set("port",$config->get("port_default"));
      $config->set("use_https",FALSE);
      $config->set("basicauth_user","");
      $config->set("basicauth_pass","");
      $config->save();
    }



    $form['host_name']=[
      '#type' => 'textfield',
      '#title' => $this->t('Prefect Server Hostname'),
      '#description' => $this->t('The DNS host name (or IP address) of the Prefect server'),

      '#default_value' =>  $config->get('host_name'),
      '#required' => true//"prefect-server"
    ];

    $form['port']=[
      '#type' => 'number',
      '#title' => $this->t('Prefect Server Port'),
      '#description' => $this->t('The network port that the Prefect Server is listening on'),
      '#default_value' => $config->get('port'),
      '#required' => true//"prefect-server"

    ];

    $form['use_https']=[
      '#type' => 'checkbox',
      '#title' =>$this->t('Use HTTPS'),
      '#description' => $this->t('Use a secure HTTPS connection to the prefect server (requires a valid SSL/TLS certificate to be configured on the Prefect Server).'),
      '#default_value' => $config->get('use_https') // 0
    ];


    $form['basicauth_user']=[
      '#type' => 'textfield',
      '#title' => $this->t('Basic Auth User'),
      '#description' => $this->t('Username for Basic Authentication (Optional)'),
      '#default_value' => $config->get('basicauth_user') //""
    ];

    $form['basicauth_pass']=[
      '#type' => 'textfield',
      '#title' => $this->t('Basic Auth Password'),
      '#description' => $this->t('Password for Basic Authentication (Optional)'),
      '#default_value' => $config->get('basicauth_pass') //""
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    /*if ($form_state->getValue('example') != 'example') {
      $form_state->setErrorByName('example', $this->t('The value is not correct.'));
    }*/
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $config= $this->config('rules_flow.settings');
    $config->set('host_name', $form_state->getValue('host_name'));
    $config->set('port', $form_state->getValue('port'));
    $config->set('use_https', $form_state->getValue('use_https'));
    $config->set('basicauth_user', $form_state->getValue('basicauth_user'));
    $config->set('basicauth_pass', $form_state->getValue('basicauth_pass'));

    $config->save();
    parent::submitForm($form, $form_state);
  }

}
